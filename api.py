import requests, json, re, sys, argparse, schedule, time, hmac, hashlib, ccxt, math, logging
from datetime import datetime

##Initializing parameters
root_logger= logging.getLogger()
root_logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('logging.log', 'w', 'utf-8')
formatter = logging.Formatter('[%(asctime)s] - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root_logger.addHandler(handler)
parser = argparse.ArgumentParser()
parser.add_argument("apiKey", help="APIKey for Binance", type=str)
parser.add_argument("secretKey", help="secretKey for Binance", type=str)
parser.add_argument("Usdt", help="USDT you're willing to put in this trade, no decimals", type=int)
parser.add_argument("ProfitMultiplicator", help="How much profit you expect to make (x2, x3...), no decimals", type=int)
args = parser.parse_args()

def DoAtXh56():
    ##Get binance tickers
    open('tickerprices.json', 'w').close()
    logging.info("File successfully cleared")
    r = requests.get("https://api.binance.com/api/v3/ticker/price")
    if r.status_code != 200:
        print(str(r.status_code))
        logging.critical("FATAL ERROR FROM API " + r.status_code)
        raise Exception('STOP EVERYTHING')
    symbols = json.loads(r.text)
    cleantickerlist = CleanPriceTicker(symbols)
    with open('tickerprices.json', 'w') as json_file:
        json.dump(cleantickerlist, json_file)
        logging.info("Ticker prices dumped into file")

def DoAtXh58():
    ##Loop till you get your trades
    querynumb = 0
    tobuy = False
    tobuymarket = []
    with open('tickerprices.json') as reader:
        filetickers = json.load(reader)
    while querynumb < 240 and tobuy is False:
        #timestamp = datetime.timestamp(now)
        res = requests.get("https://api.binance.com/api/v3/ticker/price")
        if res.status_code != 200:
            print(str(res.status_code))
            logging.critical("FATAL ERROR FROM API " + str(res.status_code))
            raise Exception('STOP EVERYTHING')
        cleantickerlist = CleanPriceTicker(json.loads(res.text))
        logging.debug("Number of tickers from file: " + str(len(filetickers)))
        logging.debug("Number of tickers from API: " + str(len(cleantickerlist)))
        if len(filetickers) != len(cleantickerlist):
            tobuymarket.append({"symbol":cleantickerlist[-1]['symbol'], "price":cleantickerlist[-1]['price']})
            tobuy = True
        else:
            querynumb += 1
            time.sleep(1)
            tobuymarket.append({"symbol":cleantickerlist[-1]['symbol'], "price":cleantickerlist[-1]['price']})
    if tobuy:
        exchange = ccxt.binance()
        quantity = float(args.Usdt)/float(tobuymarket[0]['price'])
        logging.info("Buying " + tobuymarket[0]['symbol'] + " at price " + str(tobuymarket[0]['price']) + " for a quantity of " + str(quantity))
        querystring = "symbol=" + tobuymarket[0]['symbol'] + "&side=BUY&type=MARKET&quantity=" + str(math.floor(quantity)) + "&recvWindow=60000&timestamp=" + str(exchange.public_get_time()['serverTime'])
        logging.debug("Math floor quantity " + str(math.floor(quantity)))
        secretkey = bytearray(args.secretKey, 'utf-8')
        signature = hmac.new(secretkey, querystring.encode('utf-8'), digestmod=hashlib.sha256)
        querystring += "&signature=" + str(signature.hexdigest())
        buymarketrequest = requests.post("https://api.binance.com/api/v3/order", data=querystring, headers={"X-MBX-APIKEY":args.apiKey})
        jsonbuymarketrequest = json.loads(buymarketrequest.text)
        if buymarketrequest.status_code == 200:
            sellprice = float(jsonbuymarketrequest[0]['price'])*float(args.ProfitMultiplicator)
            logging.info("Market buy response:")
            logging.info(jsonbuymarketrequest)
            logging.info("Bought " + str(jsonbuymarketrequest[0]['executedQty']) + " " + jsonbuymarketrequest[0]['symbol'] + " for " + str(float(jsonbuymarketrequest[0]['price'])*float(jsonbuymarketrequest[0]['executedQty'])) + " USDT")
            logging.info("Selling " + str(jsonbuymarketrequest[0]['symbol']) + " for a price of " + str(round(sellprice,2)) + " and quantity of " + str(jsonbuymarketrequest[0]['executedQty']))
            querystring = "symbol=" + jsonbuymarketrequest[0]['symbol'] + "&side=SELL&type=LIMIT&timeInForce=GTC&price=" + str(round(sellprice,2)) + "&quantity=" + str(jsonbuymarketrequest[0]['executedQty']) + "&recvWindow=60000&timestamp=" + str(exchange.public_get_time()['serverTime'])
            #querystring = "symbol=" + "LITUSDT" + "&side=SELL&type=LIMIT&timeInForce=GTC&price=" + "12" + "&quantity=" + "10" + "&recvWindow=60000&timestamp=" + str(exchange.public_get_time()['serverTime'])
            signature = hmac.new(secretkey, querystring.encode('utf-8'), digestmod=hashlib.sha256)
            querystring += "&signature=" + str(signature.hexdigest())
            selllimitrequest = requests.post("https://api.binance.com/api/v3/order", data=querystring, headers={"X-MBX-APIKEY":args.apiKey})
            jsonselllimitrequest = json.loads(selllimitrequest.text)
            if selllimitrequest.status_code == 200:
                logging.info("Limit sell response:")
                logging.info(jsonselllimitrequest)
            else:
                logging.critical("FATAL ERROR FROM API " + str(selllimitrequest.status_code))
                logging.critical(jsonselllimitrequest)
        else:
            logging.critical("FATAL ERROR FROM API " + str(buymarketrequest.status_code))
            logging.critical(jsonbuymarketrequest)
    else:
        logging.info("No buy attempted. No new coins added")

def CleanPriceTicker(symbols):
    ticker_list = []
    for symbol in symbols:
        if re.match(r"\b(?=\w*(USDT))\w+\b",symbol["symbol"]) and not re.match(r"\bUSDT.*\b",symbol["symbol"]) and not re.match(r"\b(?=\w*(DOWN))\w+\b",symbol["symbol"])\
            and not re.match(r"\b(?=\w*(UP))\w+\b",symbol["symbol"]) and not re.match(r"\b(?=\w*(BEAR))\w+\b",symbol["symbol"]) and not re.match(r"\b(?=\w*(BULL))\w+\b",symbol["symbol"]):
            ticker_list.append({"symbol":symbol["symbol"],"price":symbol["price"]})
    return ticker_list

#DoAtXh56()
#DoAtXh58()
logging.info("Launched succesfully")
schedule.every().day.at("04:56").do(DoAtXh56)
schedule.every().day.at("04:58").do(DoAtXh58)
schedule.every().day.at("05:56").do(DoAtXh56)
schedule.every().day.at("05:58").do(DoAtXh58)
schedule.every().day.at("06:56").do(DoAtXh56)
schedule.every().day.at("06:58").do(DoAtXh58)
schedule.every().day.at("07:56").do(DoAtXh56)
schedule.every().day.at("07:58").do(DoAtXh58)
schedule.every().day.at("08:56").do(DoAtXh56)
schedule.every().day.at("08:58").do(DoAtXh58)
schedule.every().day.at("09:56").do(DoAtXh56)
schedule.every().day.at("09:58").do(DoAtXh58)

while True:
    schedule.run_pending()
    time.sleep(1)

#print(r.text)