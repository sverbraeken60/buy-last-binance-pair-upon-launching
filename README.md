# Buy last Binance pair upon launching



## Getting started

This scripts automatically gets every crypto/USDT ticker and its prices in Binance at 56 minutes past the hour every hour and places a buy order with a sell limit specified by the user.

The script defines two functions, DoAtXh56 and DoAtXh58, which are scheduled to run at specific times (presumably 56 and 58 minutes past the hour, respectively). The DoAtXh56 function makes an API call to Binance to get the ticker prices, and then calls another function CleanPriceTicker to clean the data, before writing it to a file. The DoAtXh58 function reads the ticker prices from the file and compares it with the ticker prices received from the API. If the number of tickers from the file is not equal to the number of tickers from the API, it buys the last ticker symbol at the last ticker's price. If the number of tickers is equal after several queries, it sleeps for a second and retries.

It then uses the ccxt library to interact with the Binance exchange and places a market buy order for the symbol with the specified quantity and at the specified price. The order is placed by creating a query string with the necessary parameters and sending it as a request to the Binance API.

## Installation

Install all requirements by launching this command.

```
pip install -r requirements.txt
```

## Launching

Launch this script by putting these variables:

- apiKey: APIKey for Binance
- secretKey: secretKey for Binance
- Usdt: USDT you're willing to put in this trade, no decimals
- ProfitMultiplicator: How much profit you expect to make (x2, x3...), no decimals

## Authors and acknowledgment
Written by Steven VERBRAEKEN

## Project status
Last launched in 2021
